#include<stdio.h>
#include<stdlib.h>

int main(){
	int array_length;
	unsigned long *arr, i, sum = 0;

	scanf("%d", &array_length);
	arr = malloc(sizeof(unsigned long) * array_length);
	for(i = 0; i < array_length; i++){
		scanf("%ld", &arr[i]);
		sum += arr[i];
		}
	printf("%ld", sum);
	return 0;
	}
