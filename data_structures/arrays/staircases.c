#include<stdio.h>
#include<stdlib.h>
/* this is a program to print staircases
example N = 4
output:
   #  
  ##
 ###
####
*/

void print_staircase(int height){
	int i, j, count = height - 1;


	for(i = 0; i < height; i++){
		for(j = 0; j < height; j++){
			if(j  >= count)
				printf("#");
			else
				printf(" ");
			}
			count--;
			printf("\n");
		}
	
	
	}

int main(){
	int height;
	scanf("%d", &height);
	print_staircase(height);

	return 0;
	}
