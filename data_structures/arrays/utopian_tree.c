#include<stdio.h>
#include<stdlib.h>
/*utopian tree increases in height by 1m every summer and doubles its height every spring*/

int* calculate_tree_height(int arr[], int num_test_cases){
		int i, *height_of_tree, j;
		height_of_tree = malloc(sizeof(int) * num_test_cases);

		for(i = 0; i < num_test_cases; i++){
			height_of_tree[i] = 1;
			for(j = 0; j < arr[i]; j++){
				if(j % 2 == 0)
					height_of_tree[i] *= 2;
				else
					height_of_tree[i]++;
				}
			}
			
		return height_of_tree;
	}
int main(){
	int num_test_cases, *arr, *answer, i;
	scanf("%d", &num_test_cases);
	arr = malloc(sizeof(int) * num_test_cases);
	for(i = 0; i < num_test_cases; i++){
			scanf("%d", &arr[i]);
		}
	answer = calculate_tree_height(arr, num_test_cases);
	for(i = 0; i < num_test_cases; i++)
		printf("%d\n", *(answer + i));
	
	return 0;
	}

