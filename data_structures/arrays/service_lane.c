#include<stdio.h>
#include<stdlib.h>

/* given the entry point and ext point of Calvin's vehicle in the service lane, output the type of largest vehicle (bike, car, truck) which can pass through the service lane
-the width of the service lane varies from [1,3]
the service lane is divided into N segments, each segment having equal length but different width 
input format: N T, N= number of segments on the service road, T = number of test cases
	N separated integers which represents the width array
	entry and exit points*/

int calculate_max_vehicle_width(int width[], int entry_point, int exit_point){
	int i, min = 9999;
	for(i = entry_point; i <= exit_point; i++){
		if(width[i] < min)
			min = width[i];
		}
		return min;
	}

int main(){
	int num_test_cases, service_lane_segments, *width, i, *answer;
	int entry_point, exit_point;
	
	scanf("%d %d", &service_lane_segments, &num_test_cases);
	
	width = malloc(sizeof(int) * service_lane_segments);
	answer = malloc(sizeof(int) * num_test_cases);	//this array is to hold the max vehicle width allowed

	
	for(i = 0; i < service_lane_segments; i++)
		scanf("%d", &width[i]);
	for(i = 0; i < num_test_cases; i++){
		scanf("%d %d", &entry_point, &exit_point);
		answer[i] = calculate_max_vehicle_width(width, entry_point, exit_point);
		}
	//print the answer array
	for(i = 0; i < num_test_cases; i++)
		printf("%d\n", answer[i]);

	return 0;
	}
