#include<stdio.h>
#include<stdlib.h>
/* this is code to calculate the absolute difference between the diagonals 
of NxN matrix */

int diagonal_difference(int dimension){
	int **matrix, i, j, sum_1st_diagonal = 0, sum_2nd_diagonal = 0;
	matrix = malloc(sizeof(int *) * dimension);
	
	for(i = 0; i < dimension; i++){
		matrix[i] = malloc(sizeof(int) * dimension);		
		}
	/*so the matrix is ready now we need to input data*/
	for(i = 0; i < dimension; i++){
		for(j = 0; j < dimension; j++)
			scanf("%d", &matrix[i][j]);
		}

	/* reading of values is done
	now the diagonal difference logic will begin
	-so if row index == column index it belongs to the first diagonal
	-if row index + column index +1 == dimension then it belongs to the second diagonal*/
	
	for(i = 0; i < dimension; i++){
		for(j = 0; j < dimension; j++){
			if(i == j)
				sum_1st_diagonal += matrix[i][j];
			if((i + j + 1) == dimension)
				sum_2nd_diagonal += matrix[i][j];
			}
		}
	
	return (abs(sum_1st_diagonal - sum_2nd_diagonal));
		
	}

int main(){
	int dimension, answer;
	scanf("%d", &dimension);
	answer = diagonal_difference(dimension);
	printf("%d", answer);
		
	return 0;
	}
