#include<stdio.h>
#include<stdlib.h>

/*in this program i need to read a 6x6 array 
after reading the input I need to detect an hourglass within the array
then i need to store the sum
detect all the hourglasses and print the highest sum*/

int calculate_hourglass_highest_sum(int arr[6][6]){
	/*approach: detect the first non zero value and if it is not the 2nd last or the last element in that row then we begin with the sum calculate*/
	int i, j, *sum, k = 0;
	int max = -9999;
	sum = malloc(sizeof(int) * 20); //cannot exceed 12
	for(i = 0; i < 20; i++)
		sum[i] = 0;

	for(i = 0; i < 6; i++){
		for(j = 0; j < 6; j++){
			if(/*arr[i][j] != 0 &&*/ (((i+1) < 5)) && (j+1 < 5)){ 	/* checking the boundary conditions of i and j*/
				/*begin with the sum calculation*/
				sum[k] = arr[i][j] + arr[i][j+1] + arr[i][j+2] + arr[i+1][j+1] + arr[i+2][j] + arr[i+2][j+1] + arr[i+2][j+2];
//				printf("\nadding %d+%d+%d+%d+%d+%d+%d", arr[i][j], arr[i][j+1], arr[i][j+2], arr[i+1][j+1], arr[i+2][j], arr[i+2][j+1], arr[i+2][j+2]);
//printf("\n--%d--i = %d : --j = %d", sum[k], i, j);
				k++;
				}
			}
		}

	//find the max value from the sum array
//	printf("\nsum[1] = %d", sum[15]);

	for(i = 0; i <= (k-1); i++){
//	printf("\nsum = %d", sum[i]);

		if(sum[i] > max){
				max = sum[i];
			}
		}
		free(sum);
	return max;
	
	
	}

int main(){
	int arr[6][6], i, j, answer;

	for(i = 0; i < 6; i++){
		for(j = 0; j < 6; j++)
			scanf("%d", &arr[i][j]);
		}

	answer = calculate_hourglass_highest_sum(arr);
	printf("%d", answer);
	return 0;
	}
