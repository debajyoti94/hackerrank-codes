#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

/* the angry professor decides to cancel the class if there are less than K students based on their arrival times
input format: T - number of test cases
		N K - N= no of students in class, K= threshold count
		Eg: -1 -2 2 2 - time of arrival
		-ve numbers indicate arriving before the class starts*/


int main(){
	bool *is_class_cancelled;
	int test_cases_count, i = 0, j, num_students, threshold_count, *arrival_time, count = 0;

	scanf("%d", &test_cases_count);
	/* the loop should run till the number of test cases
	*/
	is_class_cancelled = malloc(sizeof(bool) * test_cases_count);

	while(i < test_cases_count){
		scanf("%d %d", &num_students, &threshold_count);
		/* */
		arrival_time = malloc(sizeof(int) * num_students);
		for(j = 0; j < num_students; j++){
			scanf("%d", &arrival_time[j]);
		//	printf("%d", arrival_time[i]);
			}

	/*	for(j = 0; j < num_students; j++)
			printf("%d", arrival_time[j]);
			*/
		for(j = 0; j < num_students; j++){
			if(arrival_time[j] <= 0){
			//	printf("\nearly: %d", j);
				count++;
			}
		}

			if(count >= threshold_count)
				is_class_cancelled[i] = false;
			else
				is_class_cancelled[i] = true;
		
		i++;
		count = 0;
		free(arrival_time);
		}

		for(i = 0; i < test_cases_count; i++){
			if(is_class_cancelled[i])
				printf("YES\n");
			else
				printf("NO\n");
			
			}
	free(is_class_cancelled);

	return 0;
	}
