#include<stdio.h>
#include<stdlib.h>
#include<math.h>
/* given an 2 numbers A and B find the number of perfect squares between them

we will be using the babylonian method to find out the square root of a number*/
/*float find_sqrt(int num){
	* this is the babylonian method to find sqrt of a number
	here we need to start with an approximate value, it should be closer to the root
	x = N, 
	now we need to choose another value Y which should be able to counter our approximate value
	Y = 1
	we choose an error value , e = 0.0001*

	float x = num;
	float y = 1;
	float e = 0.000001;

	while((x - y) > e){
		x = (x + y) / 2;
		y = num / x;

		}

	
	return x;
	}
*/
int sherlock_squares(int num1, int num2){
	int count = 0, i;
	float num_sqrt;

	for(i = num1; i <= num2; i++){
	//	num_sqrt = find_sqrt(i);
		num_sqrt = sqrtf(i);
		if(ceilf(num_sqrt) == num_sqrt || floorf(num_sqrt) == num_sqrt)	{
		printf("\nanswer = %f", num_sqrt);
			count++;
		}
		}

	return count;
	}

int main(){
	int num_test_cases, *count_arr, i, num1, num2;
	scanf("%d", &num_test_cases);
	count_arr = malloc(sizeof(int) * num_test_cases);

	for(i = 0; i < num_test_cases; i++){
		scanf("%d %d", &num1, &num2);
		count_arr[i] = sherlock_squares(num1, num2);
	}
	
	for(i = 0; i < num_test_cases; i++)
		printf("%d\n", count_arr[i]);
	return 0;
	}
