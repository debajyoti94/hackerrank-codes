#include<stdio.h>
#include<stdlib.h>
/* this is a program to calculate the largest Decent Number*/

void decent_number(int num_test_cases){
	int i, j;
	unsigned long *input;
	int dc_num = 1, **arr, count_of_3 = 0, count_of_5 = 0, *status;

	/* store a 10000 digit number in an array*/
	
	arr = malloc(sizeof(int*) * num_test_cases);
	input = malloc(sizeof(unsigned long) * num_test_cases); // max no
	status = malloc(sizeof(int) * num_test_cases);

	for(i = 0; i < num_test_cases; i++){
			scanf("%ld", &input[i]);
		//	printf("\ninput = %ld", input[i]);
			arr[i] = malloc(sizeof(int) * 100000); //largest test case will have at most 1000000 digits
			/*now the logic for decent number will begin*/
		/*the logic goes like this
		-fill the entire array with 5's
		-go backwards fill 5 times 3 and check whether (count_5 % 3 && count_3 % 5 == 0)*/
			count_of_5 = input[i];
			for(j = 0; j < input[i]; j++)
				arr[i][j] = 5;

			if(input[i] % 3 == 0){
				//printf("\nentering");
					status[i] = 1;
					continue;

					}
			else{
				/* go in the reverse order and keep appending 3's till u get a balance on no: of 5 and no: of 3*/

				status[i] = 0;
				j = j - 1;
				while(j >= 0){
						arr[i][j] = 3;
						count_of_3++;
						count_of_5--;
						j--;
					//	printf("\n%d:%d:%d", count_of_3, count_of_5, j);
						if((count_of_3 % 5 == 0) && (count_of_5 % 3 == 0 )){
							status[i] = 1;	
							break;
						}
						

					
					}

				}
				count_of_3 = 0;
		
		}

		for(i = 0; i < num_test_cases; i++){
			for(j = 0; j < input[i]; j++){
				if(status[i] == 1)
					printf("%d", arr[i][j]);
				else{
					printf("-1");
					break;
				}
			}
			printf("\n");
			}
	
	}

int main(){
	int num_test_cases;
	scanf("%d", &num_test_cases);
	decent_number(num_test_cases);

	return 0;	
	}
