#include<stdio.h>
#include<stdlib.h>
#include<string.h>
/*in this program we have to enter N strings. Each string's length is no more
than 20 chars. There are also Q queries. For each query you are given a string, and you need to find out how many times this string occured previously.
*/

int reckon_occurrence(char **strings_arr, char *query, int num_of_strings){
//	printf("\n%s:%s",__FUNCTION__, query);
	int count = 0, i, j;
	/*here i need to find how many times does the query string occur in the strings_arr and return the value*/
	for(i = 0; i < num_of_strings; i++){
		if(strcmp(query, strings_arr[i]) == 0)
			count++;
		}
	//	printf("\ncount = %d", count);
	return count;
	}

int main(){
	int num_of_strings, i, j, num_of_queries, *count;
	char **strings_arr, **query;
	
	scanf("%d", &num_of_strings);

	strings_arr = malloc(sizeof(char *) * num_of_strings);
	for(i = 0; i < num_of_strings; i++)
		strings_arr[i] = malloc(sizeof(char) * 20);

//accepting input
	
	for(i = 0; i < num_of_strings; i++)
		scanf("%s", strings_arr[i]);

//accepting queries
	scanf("%d", &num_of_queries);
	query = malloc(sizeof(char*) * num_of_queries);
	for(i = 0; i < num_of_queries; i++)
		query[i] = malloc(sizeof(char) * 20);
//memory allocated, time to accept queries
	count = malloc(sizeof(int) * num_of_queries);

	for(i = 0; i < num_of_queries; i++){
		scanf("%s", query[i]);
		count[i] = reckon_occurrence(strings_arr, query[i], num_of_strings);
	}
		
//will create a count array which will keep track of the count of each string that was passed in the query

	for(i = 0; i < num_of_queries; i++){
		printf("%d\n", count[i]);
		free(query[i]);
	}
	free(query);
		
	free(count);
	for(i = 0; i < num_of_strings; i++){
		free(strings_arr[i]);	
	}
	free(strings_arr);
/*	printf("\nInput\n");
	for(i = 0; i < num_of_strings; i++)
		printf("%s\n", strings_arr[i]);
*/	return 0;
	}
