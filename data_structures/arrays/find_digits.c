#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<stdbool.h>
/* this is a program to find out which digits of a number N evenly divide N
input may vary from 0 to 10^9*/

int find_digits(char num[], int length){
	int *number, i = 0, j = 0, sum = 0, count = 0;
	bool *is_num_already_checked;

	is_num_already_checked = malloc(sizeof(bool) * 10);
	number = malloc(sizeof(int) * length);
	
	for(i = 0; i < length; i++){
		number[i] = num[i] - '0';
		sum += number[i];
		//printf("%d", number[i]);
	}
	for(j = 0; j < 10; j++)
		is_num_already_checked[j] = false;

	i--;
	/*here the divisibility test will begin*/
	for(j = 0; j < length; j++){
		int temp = 1;
		switch(number[j]){
			default: break;
			case 1:// printf("\nEntering 1");
				count++;
				break;
			case 2:// printf("\nEntering 2");
				if(is_num_already_checked[number[j]])
					count++;
				else{	
					if(number[i] == 0 || number[i] == 2 || number[i] == 4 || number[i] == 6 || number[i] == 8 ){
						count++;
						is_num_already_checked[number[j]] = true;
						}
					}
				break;
			case 3: //printf("\nEntering 3");
				if(is_num_already_checked[number[j]])
					count++;
				else{
					if(sum % 3 == 0){
						count++;
						is_num_already_checked[number[j]] = true;
						}
					
				}
					break;
			case 4: 
				/*here we need the last 2 digits and check whether it is divisible by 4*/
		//	printf("\nEntering 4");
				if(is_num_already_checked[number[j]])
					count++;
				else{	
					temp = (number[i - 1] * 10) + number[i];  
					if(temp % 4 == 0){
						count++;
						is_num_already_checked[number[j]] = true;
						}
					
					}
					break;
			case 5://printf("\nEntering 5");
				/* here we check if the last digit is 0 or 5*/
				if(is_num_already_checked[number[j]])
					count++;
				else{
					if(number[i] == 0 || number[i] == 5){
						count++;
						is_num_already_checked[number[j]] = true;
						}
					}
					break;
			case 6://printf("\nEntering 6");
				/*check if divisible by 2 and 3*/
				if(is_num_already_checked[number[j]] == true)
						count++;
			else{
				if(is_num_already_checked[2] == true && is_num_already_checked[3] == true)
					count++;
				else{
					if((number[i] == 0 || number[i] == 2 || number[i] == 4 || number[i] == 6 
							|| number[i] == 8) && sum % 3 == 0){
							count++;
							is_num_already_checked[number[j]] = true;
							}
						
					}
				}
				break;

			case 7:
				/*form alternate blocks of 3 digits then add subtract add ....*/
				if(is_num_already_checked[number[j]] == true)
					count++;
				else{
					int index = i, k = 0, sum_7 = 0, block_of_3, flag = 0;
					int num1, num2, num3;
					for(k = 0; k < length;){
							num1 = k++ < length? (number[index--]) : 0; 
							num2 = k++ < length? (number[index--]) : 0;
							num3 = k++ < length? number[index--] : 0;
							block_of_3 = num1 + (num2 * 10) + (num3 * 100);
							if(flag == 0){
								sum_7 += block_of_3;
								flag = 1;
							}
							else{	sum_7 -= block_of_3;
								flag = 0;
								
								}

						}
						if(sum_7 % 7 == 0){
							count++;
							is_num_already_checked[number[j]] = true;
							}					
					}
				break;

			case 8://printf("\nEntering 8");
				/* check if last 3 digits is divisible by 8*/
				if(is_num_already_checked[number[j]] == true)
					count++;
				else{
					temp = (number[i-2] * 100) + (number[i-1] * 10) + number[i];
					if(temp % 8 == 0)
						count++;
					
					}
				break;
			case 9://printf("\nEntering 9");
				if(is_num_already_checked[number[j]] == true)
					count++;
				else{
					if(sum % 9 == 0)
						count++;
					}
				break;
			} // end of switch case
		
		}// end of for loop

		return count;


	}

int main(){
	int num_test_cases, i = 0, *count, length = 0;
	char num[10];

	scanf("%d", &num_test_cases);
	count = malloc(sizeof(int) * num_test_cases);

	while(i < num_test_cases){
		/*plan is to read numbers in a character array and then*/
		scanf("%s", num);
		length = strlen(num);
		count[i] = find_digits(num, length);	
		i++;
		}
	
	for(i = 0; i < num_test_cases; i++)
		printf("%d\n", count[i]);		

	return 0;
	}
