#include<stdio.h>
#include<stdlib.h>
#include<math.h>
/* this program is to calculate the fraction of +ve, -ve and zeros in an array
- the fraction value should be upto 6 decimal places
-traverse the array while determining whether it should be in the +ve value / negative value / zero value
*/
double fraction[3];

double* calculate_fraction(int length){
	int i, *arr;
	double plus_fraction = 0, minus_fraction = 0, zero_fraction = 0;
	
	arr = malloc(sizeof(int) * length);

	for(i = 0; i < length; i++){
		scanf("%d", &arr[i]);
		if(arr[i] > 0)	
			plus_fraction++;
		else if(arr[i] < 0)
			 minus_fraction++;
		else
			zero_fraction++;
		}	
	fraction[0] = (plus_fraction / length);
	fraction[1] = (minus_fraction / length);
	fraction[2] = (zero_fraction / length);
	
		return fraction;
	}

int main(){
	int length, i = 0;
	double *ptr;

	scanf("%d", &length);
	ptr = calculate_fraction(length);
	for(;i < 3; i++)
		printf("%.6lf\n", *(ptr + i));

	return 0;
	}
