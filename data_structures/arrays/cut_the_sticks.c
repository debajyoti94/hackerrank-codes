#include<stdio.h>
#include<stdlib.h>
#include<stdbool.h>

void print_number_of_sticks_left(int arr[], int num_sticks){
	/* here i need to subtract the 1st element value from the rest of the array and go on till the entire array turns 0*/
	int i, smallest_stick = arr[0], count = 0, next_smallest_stick;	//just to start with the first element
	bool sticks_left = true;
	
	printf("%d\n", num_sticks);
/*	for(i = 0; i < num_sticks; i++)
		printf("\ninput array = %d\n", arr[i]);
*/
	while(sticks_left){
			 sticks_left = false;

		 		 //subtracting values from the sticks and checking if any exist
		 for(i = 0; i < num_sticks; i++){
		//	 printf("\nsmallest_stick = %d", smallest_stick);
			 arr[i] = arr[i] - smallest_stick;
			 if(arr[i] > 0){
				 if(count == 0)
					 next_smallest_stick = arr[i]; //since it will be the first time we are encountering a non zero value it has to be the smallest
				 sticks_left = true;
			 	 count++;
				}
			 }
			 /*tasks left
			 -- need to change the value of smallest stick
			 --need to start the loop again from the next non zero positive value to reduce the iterations*/
		/*	 for(i = 0; i < num_sticks; i++)
				 printf("--(%d)", arr[i]);
		*/	 if(count > 0)
			 	printf("%d\n", count);

			 
		         count = 0;
			 smallest_stick = next_smallest_stick;

		}
		
	
	}

int main(){
	int num_of_sticks, *sticks_arr, i, min = 9999, temp, j, answer;
	scanf("%d", &num_of_sticks);
	
	sticks_arr = malloc(sizeof(int) * num_of_sticks);
	for(i = 0; i < num_of_sticks; i++){
		scanf("%d", &sticks_arr[i]);
		for(j = 0; j < i; j++){
			if(sticks_arr[i] < sticks_arr[j]){
			//swap 
			temp = sticks_arr[i];
			sticks_arr[i] = sticks_arr[j];
			sticks_arr[j] = temp;
			}
		}
	}

		print_number_of_sticks_left(sticks_arr, num_of_sticks);
		//printf("%d\n", answer);

	free(sticks_arr);
	return 0;
	}
