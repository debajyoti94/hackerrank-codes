#include<stdio.h>
#include<stdlib.h>
#include<string.h>

/*in this program we have to convert a string which can contain only A's and B's to a string in which we have an alternating sequence
-we can delete characters from the string to get this format
eg: input: AAABBBA
this string should be converted to ABA and calculate the number of deletions made
Display this deletion count..*/

int funny_characters(char input[], int length){
//	printf("\n%s : %s : %d", __FUNCTION__, input, length);
	int count = 0, i, j;
	for(i = 0, j = 1; j < length; i++, j++){
	//	printf("\n%c:%c", input[i], input[j]);
		if(input[i] == input[j]){
		//	input[j] = '-';
			count++;			
		//	printf("\n%d:%d:%d", i, j, count);
			}
		}
		return count;
	}

int main(){

	int num_test_cases, i;
	int *deletion_count;
	char input[100000];

	scanf("%d", &num_test_cases);

	deletion_count = malloc(sizeof(int) * num_test_cases);
	
	for(i = 0; i < num_test_cases; i++){
		scanf("%s", input);
		int length = strlen(input);
		deletion_count[i] = funny_characters(input, length);	
		
		}
	for(i = 0; i < num_test_cases; i++)
		printf("%d\n", deletion_count[i]);	
	return 0;
	}
