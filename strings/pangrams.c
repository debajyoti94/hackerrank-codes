/* Pangram is a sentence constructed by using every letter of the alplhabet at least once
Given a sentence s, tell Roy if it is a pangram or not*/

#include<stdio.h>
#include<stdbool.h>

int main(){
/*read a string into a 2d array then parse through the 2d array and find out how many alphabets are covered*/

	bool alphabets[26];
	char buf[1000];
	int i = 0;

	fgets(buf, sizeof(buf), stdin);

//	printf("\ninput = %s", buf);

	while(buf[i] != '\0'){
		
		switch(buf[i]){
			case 'A':
			case 'a':
				alphabets[0] = true;
				break;				
			case 'B':
			case 'b':
				alphabets[1] = true;
				break;	
			case 'C':
			case 'c':
				alphabets[2] = true;
				break;	
			case 'D':
			case 'd':
				alphabets[3] = true;
				break;	
			case 'E':
			case 'e':
				alphabets[4] = true;
				break;	
			case 'F':
			case 'f':
				alphabets[5] = true;
				break;	
			case 'G':
			case 'g':
				alphabets[6] = true;
				break;	
			case 'H':
			case 'h':
				alphabets[7] = true;
				break;	

			case 'I':
			case 'i':
				alphabets[8] = true;
				break;		
      		        case 'J':
			case 'j':
				alphabets[9] = true;
				break;		
		
			case 'K':
			case 'k':
				alphabets[10] = true;
				break;			
				
			case 'L':
			case 'l':
				alphabets[11] = true;
				break;	
			case 'M':
			case 'm':
				alphabets[12] = true;
				break;
			case 'N':
			case 'n':
				alphabets[13] = true;
				break;		
			case 'O':
			case 'o':
				alphabets[14] = true;
				break;	
			case 'P':
			case 'p':
				alphabets[15] = true;
				break;											
			
			case 'Q':
			case 'q':
				alphabets[16] = true;
				break;	
			case 'R':
			case 'r':
				alphabets[17] = true;
				break;		
				
			case 'S':
			case 's':
				alphabets[18] = true;
				break;	
			case 'T':
			case 't':
				alphabets[19] = true;
				break;
			case 'U':
			case 'u':
				alphabets[20] = true;
				break;	
			case 'V':
			case 'v':
				alphabets[21] = true;
				break;	
			case 'W':
			case 'w':
				alphabets[22] = true;
				break;							
					
			case 'X':
			case 'x':
				alphabets[23] = true;
				break;							
			case 'Y':
			case 'y':
				alphabets[24] = true;
				break;			
				
			case 'Z':
			case 'z':
				alphabets[25] = true;
				break;		
				
			}
			i++;

		}	for(i = 0; i < 26; i++){
				if(alphabets[i] == false){
					printf("not pangram");
					return 0;
					}
					
				}
		
	
		
		printf("pangram");


	return 0;
	}
