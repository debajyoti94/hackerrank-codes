#!/bin/bash
#script to check whether a triangle is scalene or equi or iso
#user gives input: 3 sides of the triangle
read side1
read side2		#that is how read input using stdin
read side3

#check whether it is a triangle or not

if [ "$(($side1 + $side2))" -gt "$side3" ] && [ "$(($side1 + $side3))" -gt "$side2" ] && [ "$(($side2 + $side3))" -gt "$side1" ] #here what i learnt was how to evaluate the sum of two numbers and return that value
then	
	if [ "$side1" -eq "$side2" ] && [ "$side2" -eq "$side3" ] && [ "$side3" -eq "$side1" ]
	then
		echo "EQUILATERAL"
	elif [ "$side1" -eq "$side2" ] || [ "$side2" -eq "$side3" ] || [ "$side3" -eq "$side1" ]
	then
		echo "ISOSCELES"
	else
		echo "SCALENE"
	fi
else
	echo "Wrong input"
fi

