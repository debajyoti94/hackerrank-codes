#!/bin/bash

read length
i=0
sum=0
for i in `seq 1 $length`;
do
	read num
	sum=$(($num + $sum)) 
done


echo "scale = 3; $sum / $length" | bc -l
