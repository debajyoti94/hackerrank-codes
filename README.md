This repository contains all the codes of the problems that i have been solving at hackerrank.
The codes belong to the following domains:
-Bash
-Algorithms
-Data Structures (Arrays currently)
-String manipulation

All the codes have been written in C.